const gulp = require('gulp');
const nodemon = require('gulp-nodemon');
const notify = require('gulp-notify');
const livereload = require('gulp-livereload');

// Task
gulp.task('default', function () {
    // listen for changes
    livereload.listen();
    // configure nodemon
    nodemon({
        // the script to run the server
        script: 'server.js',
        ext: 'js'
    }).on('restart', function () {
        // when the server has restarted, run livereload.
        gulp.src('server.js')
            .pipe(livereload())
        //.pipe(notify('Reloading page, please wait...'));
    })
})