const express = require('express');
const mongoose = require('mongoose');
const helmet = require('helmet');
const cors = require('cors');
const morgan = require('morgan');
const methodOverride = require('method-override');
const http = require('http');
const errorHandler = require('errorhandler');
const path = require('path');
const bodyParser = require('body-parser');
const app = express();

//Default Express Config
app.use(cors());
app.use(helmet());
app.use(morgan('dev'));
app.use(methodOverride());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//Serve files
app.use(express.static(path.join(__dirname, 'public')));

//Database Connection
mongoose.connect('mongodb://localhost:27017/medicrmdb');
mongoose.set('debug', true)

const dbConnect = mongoose.connection;
dbConnect.on('error', console.error.bind(console, 'connection error'));
dbConnect.once('open', console.log.bind(console, 'connection success'));

http.createServer(app).listen(3001, function () {
    console.log('Magic happens on http://localhost:3001');
});